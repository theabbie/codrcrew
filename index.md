---
title: Home
sections:
  - section_id: intro
    component: intro.html
    type: intro
    title: Intro
    content: Playing with life since 2002
menus:
  main:
    title: Home
    weight: 1
layout: home
---
